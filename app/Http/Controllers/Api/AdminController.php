<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AdminResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * @return AdminResource
     */
    public function getProfile()
    {
        $authUser = Auth::user();
        return new AdminResource($authUser);
    }

}

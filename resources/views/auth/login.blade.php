<!-- Styles -->
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
@extends('layouts.app')
@section('content')
    <div class="page-container">
        <h1>登录(欢迎)</h1>
        <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
            @csrf
            <input type="text" name="username" class="username" placeholder="请输入您的用户名！" required autofocus>
            <input type="password" name="password" class="password" placeholder="请输入您的用户密码！" required>
            <button type="submit" class="submit_button">登录</button>
        </form>
    </div>
@endsection

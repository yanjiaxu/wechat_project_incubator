<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['auth:api']], function () {
    /**
     * 管理员管理
     */
    Route::get('profile', 'AdminController@getProfile');


});
Route::group(['prefix' => 'client', 'namespace' => 'Client'], function () {     //用户头像、姓名
    Route::get('wechat-js-sdk', 'WechatController@getWeChatJsSdk');
});



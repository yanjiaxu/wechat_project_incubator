<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Site','middleware'=>['auth']], function () {
    Route::get('/', 'SiteController@index');
});
Auth::routes();

Route::get('wechat/callback', 'API\Client\WechatController@callback');

/**
 * 微信支付回调
 */
Route::post('wechat/pay-callback', 'API\Client\WechatController@payCallback');
